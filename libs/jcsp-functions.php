<?php

/**
 * Fetch template location from plugin or theme
 * 
 * @param  string $template   
 * @return string
 */
function jcsp_get_template_part($template){

	$located = JCSP()->get_plugin_dir() . 'templates/'.$template.'.php';
	$template_file = get_stylesheet_directory() . '/' . JCSP()->get_plugin_slug() . '/'.$template.'.php';
	if(is_file($template_file)){
		$located = $template_file;
	}

	return $located;
}