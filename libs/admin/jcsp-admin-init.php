<?php
/**
 * Load all admin classes
 *
 * @author James Collings <james@jclabs.co.uk>
 * @version 0.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// include admin files
include_once 'class-jcsp-admin-update.php';
include_once 'class-jcsp-admin-product-settings.php';
include_once 'class-jcsp-admin-product-report.php';

/**
 * Register Set Your Price settings
 * 
 * @param  array  $pages 
 * @return array
 */
function jcsp_register_settings($pages = array()){
	$pages[] = include_once 'class-jcsp-admin-settings.php';
	return $pages;
}
add_filter( 'woocommerce_get_settings_pages', 'jcsp_register_settings' );