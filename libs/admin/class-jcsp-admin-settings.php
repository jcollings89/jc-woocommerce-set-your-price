<?php
/**
 * Set Your Price Settings 
 *
 * Add WooCommerce settings page for custom options
 *
 * @author James Collings <james@jclabs.co.uk>
 * @version 0.2
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class JCSP_Admin_Settings extends WC_Settings_Page {

	public function __construct(){
		$this->id    = 'set-your-price';
		$this->label = __( 'Set Your Price', 'woocommerce' );

		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
		add_action( 'woocommerce_sections_' . $this->id, array( $this, 'output_sections' ) );
		add_action( 'woocommerce_settings_' . $this->id, array( $this, 'output' ) );
		add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );

		add_action( 'woocommerce_sections_'.$this->id, array( $this, 'before_settings_output' ), 5 );
		add_action( 'woocommerce_settings_'.$this->id, array( $this, 'after_settings_output' ), 15 );
	}

	public function before_settings_output(){
		?>
		<style type="text/css">

		.jcsp_wrapper{
			position: relative;
		}

		.jcsp_wrapper .jcsp_right, .jcsp_wrapper .jcsp_left{
			width: 100%;
			position: static;
		}

		.jcsp_wrapper .jcsp_right{
			background: #FFF;
			border: 1px solid #CCC;
			margin-top: 10px;
		}

		.jcsp_inside{
			padding: 0 12px 12px;
			line-height: 1.4em;
			font-size: 13px;
		}

		.jcsp_inside p{
			margin:6px 0 0;
		}

		.jcsp_right h3{
			font-size: 14px;
			line-height: 1.4;
			padding: 8px 12px;
			margin: 0;
		}

		.jcsp_right ul{
			margin-bottom: 0;
		}

		.jcsp_version{
			margin:0;
			padding:10px;
			background: #5a9889;
			color: #FFF;
		}

		.jcsp_right hr{
			margin-top: 0;
		}

		@media screen and ( min-width: 782px ) {
			
			.jcsp_wrapper .jcsp_right{
				width: 250px;
				position: absolute;
				right: 0;	
				margin-top: 40px;		
			}

			

			.jcsp_wrapper .jcsp_left{
				padding-right: 270px;
				width: auto;
			}
		}

		</style>
		<div class="jcsp_wrapper">
		<div class="jcsp_right">
			<h3><span><?php _e('JC WooCommerce Set Your Price', 'jcsp'); ?></span></h3>
			<hr />
			<div class="jcsp_inside">
				<p><?php _e('Thank you for using this plugin, for more information on how to use it checkout the following links.', 'jcsp'); ?></p>
				<ul>
					<li><a href="http://jamescollings.co.uk/docs/v1/jc-woocommerce-set-price/?ref=<?php echo site_url('/'); ?>" target="_blank"><?php _e('Documentation', 'jcsp'); ?></a></li>
					<li><a href="http://jamescollings.co.uk/support/forum/jc-woocommerce-set-price/?ref=<?php echo site_url('/'); ?>" target="_blank"><?php _e('Support', 'jcsp'); ?></a></li>
					<li><a href="http://jamescollings.co.uk/wordpress-plugins/woocommerce-set-your-price/?ref=<?php echo site_url('/'); ?>" target="_blank"><?php _e('About', 'jcsp'); ?></a></li>
				</ul>
			</div>
			<p class="jcsp_version">
				Version: <strong><?php echo JCSP()->get_version(); ?></strong>
			</p>
		</div>
		<div class="jcsp_left">
		<?php
	}

	public function after_settings_output(){
		?>
		</div><!-- end of jcsp_left -->
		</div><!-- end of jcsp_wrapper -->
		<div class="clear"></div>
		<?php
	}

	/**
	 * Get sections
	 *
	 * @return array
	 */
	public function get_sections() {

		$sections = array(
			''  => __( 'Settings', 'jcsp' ),
		);

		if(get_option('jcsp_enable_labels') == 'yes'){
			$sections['label'] = __( 'Labels', 'jcsp' );
		}

		return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
	}

	/**
	 * Get settings array
	 *
	 * @return array
	 */
	public function get_settings( $current_section = '' ) {
		$settings = array();

		if ( 'label' == $current_section ) {	

			$settings[] = array(	
				'title' => __( 'General Labels', 'jcsp' ), 
				'desc' => __( 'Change the text which is displayed.', 'jcsp'),
				'type' => 'title', 
				'id' => 'jcwd_cart_label_options'
			);
			$settings[] = array(
				'title'    => __( 'Before Input', 'jcsp' ),
				'desc_tip'     => __( 'label displayed before the set your price input.', 'jcsp' ),
				'id'       => 'jcsp_general_before_input',
				'default'  => JCSP()->get_label('general', 'before_input', true), // 'Add a donation to your order:',
				'css'      => 'min-width:350px;',
				'type'     => 'text',
				'autoload' => false
			);
			$settings[] = array(
				'title'    => __( 'After Input', 'jcsp' ),
				'desc_tip'     => __( 'label displayed after the set your price input, % will be replaced by the minimum amount.', 'jcsp' ),
				'id'       => 'jcsp_general_after_input',
				'default'  => JCSP()->get_label('general', 'after_input', true), // 'Add a donation to your order:',
				'css'      => 'min-width:350px;',
				'type'     => 'text',
				'autoload' => false
			);
			$settings[] = array( 'type' => 'sectionend', 'id' => 'jcwd_cart_label_options' );	

			$settings[] = array(	
				'title' => __( 'Notice Labels', 'jcsp' ), 
				'desc' => __( 'Change the text which is displayed on notices.', 'jcsp'),
				'type' => 'title', 
				'id' => 'jcwd_cart_label_options'
			);
			$settings[] = array(
				'title'    => __( 'General Error', 'jcsp' ),
				'desc_tip'     => __( 'Notice displayed when an error has happened.', 'jcsp' ),
				'id'       => 'jcsp_notice_error',
				'default'  => JCSP()->get_label('notice', 'error', true), // 'Add a donation to your order:',
				'css'      => 'min-width:350px;',
				'type'     => 'text',
				'autoload' => false
			);
			$settings[] = array(
				'title'    => __( 'Minimum validation error', 'jcsp' ),
				'desc_tip'     => __( 'Notice displayed when an amount is less than the minimum amount.', 'jcsp' ),
				'id'       => 'jcsp_notice_min_error',
				'default'  => JCSP()->get_label('notice', 'min_error', true), // 'Add a donation to your order:',
				'css'      => 'min-width:350px;',
				'type'     => 'text',
				'autoload' => false
			);
			$settings[] = array( 'type' => 'sectionend', 'id' => 'jcwd_cart_label_options' );	

		}else{
			$settings[] = array(	
				'title' => __( 'General Settings', 'jcsp' ), 
				'desc' => __( 'Change settings to do with JC WooCommerce Set Your Price Plugin', 'jcsp'),
				'type' => 'title', 
				'id' => 'jcsp_general_options'
			);
			$settings[] = array(
				'title'    => __( 'Enable Label Override', 'jcsp' ),
				'desc'     => __( 'Enable override of text labels', 'jcsp' ),
				'id'       => 'jcsp_enable_labels',
				'default'  => 'no',
				'type'     => 'checkbox',
				'autoload' => false
			);
			$settings[] = array(
				'title'    => __( 'Enable Plugin Styles', 'jcsp' ),
				'desc'     => __( 'Enable css styles', 'jcsp' ),
				'id'       => 'jcsp_enable_css',
				'default'  => 'yes',
				'type'     => 'checkbox',
				'autoload' => false
			);
			$settings[] = array( 'type' => 'sectionend', 'id' => 'jcsp_general_options' );
		}

		return $settings;
	}

	/**
	 * Save settings
	 */
	public function save() {
		global $current_section;

		$settings = $this->get_settings( $current_section );
		WC_Admin_Settings::save_fields( $settings );
	}

	/**
	 * Output the settings
	 */
	public function output() {
		global $current_section;

		$settings = $this->get_settings($current_section);
		WC_Admin_Settings::output_fields( $settings );
	}
}

return new JCSP_Admin_Settings();