<?php
/**
 * Display set your price fields within admin area
 *
 * Allow users to enable set your price on specific products (simple & variable)
 * 
 * @author James Collings <james@jclabs.co.uk>
 * @version 0.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class JCSP_Admin_Product_Settings{
	
	public function __construct(){

		// output html in general product data tab
		add_action( 'woocommerce_product_options_pricing' , array( $this, 'display_product_settings' ) );

		// add product type option to list
		add_filter( 'product_type_options', array( $this, 'product_type_options' ) );

		// output data per variation
		add_action( 'woocommerce_product_after_variable_attributes' , array( $this, 'display_variable_product_settings' ), 10, 3 );

		// enable checkbox to hide and show set your price settings on variations
		add_action( 'woocommerce_variation_options' , array( $this, 'display_variable_enable_settings' ), 10, 3 );

		// save meta data for product
		add_action( 'woocommerce_process_product_meta', array( $this, 'save_product' ), 10, 2 );

		// save meta data for product variation
		add_action( 'woocommerce_save_product_variation', array($this, 'save_product_variation' ), 10, 2 );

		// duplicate post meta
		add_action( 'wcml_after_duplicate_product_post_meta', array( $this, 'wcml_after_duplicate_product_post_meta' ) , 10, 2);

		// WPML 
		add_filter( 'icl_cf_translate_state', array( $this, 'set_custom_field_state' ), 10, 2 );
	}	

	/**
	 * Tell WPML to copy over jcsp custom fields to translations by default
	 * @param string $response 
	 * @param string $field
	 */
	public function set_custom_field_state($response, $field){
		
		if( in_array( $field, array( '_jcsp_enabled', '_jcsp_limit_min', '_jcsp_suggested_price' ) ) ){
			return 'copy';
		}

		return $response;
	}

	/**
	 * Copy over main translated product keys for wpml sites
	 * @param  integer $original_product_id 
	 * @param  integer $trnsl_product_id                  
	 * @return void
	 */
	public function wcml_after_duplicate_product_post_meta($original_product_id, $trnsl_product_id){

		// fetch main values
		$_jcsp_enabled = get_post_meta( $original_product_id, '_jcsp_enabled', true );
		$_jcsp_limit_min = get_post_meta( $original_product_id, '_jcsp_limit_min', true );
		$_jcsp_suggested_price = get_post_meta( $original_product_id, '_jcsp_suggested_price', true );

		// set default values
		$_jcsp_enabled = $_jcsp_enabled == 'yes' ? 'yes' : 'no';

		// update meta
		update_post_meta( $trnsl_product_id, '_jcsp_enabled', $_jcsp_enabled);
		update_post_meta( $trnsl_product_id, '_jcsp_limit_min', $_jcsp_limit_min);
		update_post_meta( $trnsl_product_id, '_jcsp_suggested_price', $_jcsp_suggested_price);
	}

	/**
	 * Add settings to simple product
	 * @return void
	 */
	public function display_product_settings(){
		?>
		<script type="text/javascript">
		jQuery(function($){

			$('body').on('change', '#_jcsp_enabled', function(){
				if($(this).is(':checked')){
					$('.show_if_jcsp').show();
					$('._regular_price_field, ._sale_price_field').hide();

					var reg_price = $('#_regular_price');
					if(reg_price.val() == ''){
						reg_price.val(0);
					}

					$('body').on('input', '#_jcsp_suggested_price', function(){
						if($(this).val() > 0){
							reg_price.val($(this).val());
						}
					});

					$('body').on('input', '#_jcsp_limit_min', function(){
						if( $('#_jcsp_suggested_price').val() == '' && $(this).val() > 0){
							reg_price.val($(this).val());
						}
					});

				}else{
					$('.show_if_jcsp').hide();
					$('._regular_price_field, ._sale_price_field').show();
				}
			});

			$('#_jcsp_enabled').trigger('change');
		});
		</script>
		<?php

		// manage stock
		woocommerce_wp_text_input( array( 'id' => '_jcsp_limit_min', 'wrapper_class' => 'show_if_simple show_if_variable show_if_jcsp', 'label' => __( 'Minimum Price', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')', 'data_type' => 'price' ) );
		woocommerce_wp_text_input( array( 'id' => '_jcsp_suggested_price', 'wrapper_class' => 'show_if_simple show_if_variable show_if_jcsp', 'label' => __( 'Suggested Price', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')', 'data_type' => 'price' ) );
	}

	public function product_type_options($options){
		$options['jcsp_enabled'] = array(
			'id'            => '_jcsp_enabled',
			'wrapper_class' => 'show_if_simple',
			'label'         => __( 'Set Your Price', 'woocommerce' ),
			'description'   => __( 'Enable customers to set how much they want to pay for this', 'woocommerce' ),
			'default'       => 'no'
		);
		return $options;
	}

	/**
	 * Enable checkbox to hide and show set your price settings on variations
	 * @param  integer $loop           
	 * @param  array $variation_data 
	 * @param  array $variation      
	 * @return void
	 */
	public function display_variable_enable_settings($loop, $variation_data, $variation){
		// $_jcsp_enabled = isset($variation_data['_jcsp_enabled'][0]) ? $variation_data['_jcsp_enabled'][0] : 'no';
		
		$variation_id = $variation->ID;
		$_jcsp_enabled = get_post_meta( $variation_id, '_jcsp_enabled', true);
		if($_jcsp_enabled != 'yes'){
			$_jcsp_enabled = 'no';
		}
		
		?>
		<label><input type="checkbox" value="yes" class="checkbox jcsp_variable_enabled" name="_jcsp_variable_enabled[<?php echo $loop; ?>]" <?php checked( 'yes', $_jcsp_enabled, true ); ?> /> <?php _e( 'Set Your Price', 'woocommerce' ); ?> <a class="tips" data-tip="<?php _e( 'Enable override of set your price settings', 'woocommerce' ); ?>" href="#">[?]</a></label>
		<script type="text/javascript">
		jQuery(function($){
			$('body').on('click enable_check', '.jcsp_variable_enabled', function(){

				var _variation = $(this).parentsUntil('.woocommerce_variation');
				if($(this).is(':checked')){
					_variation.find('.show_if_variable_is_jcsp').show();
					_variation.find('input[name^="variable_regular_price"],input[name^="variable_sale_price"]').parent().hide();

					var reg_price = _variation.find('input[name^="variable_regular_price"]');
					if(reg_price.val() == ''){
						reg_price.val(0);
					}

					_variation.on('input', 'input[name^="_jcsp_variable_suggested_price"]', function(){
						if($(this).val() > 0){
							reg_price.val($(this).val());
						}
					});

					_variation.on('input', 'input[name^="_jcsp_variable_limit_min"]', function(){
						if( _variation.find('input[name^="_jcsp_variable_suggested_price"]').val() == '' && $(this).val() > 0){
							reg_price.val($(this).val());
						}
					});

				}else{
					_variation.find('.show_if_variable_is_jcsp').hide();
					_variation.find('input[name^="variable_regular_price"],input[name^="variable_sale_price"]').parent().show();
				}
			});

			$('.jcsp_variable_enabled').trigger('enable_check');
		});
		</script>
		<?php
	}

	/**
	 * Display variable product form
	 * @param  integer $loop           
	 * @param  array $variation_data 
	 * @param  array $variation      
	 * @return void
	 */
	public function display_variable_product_settings($loop, $variation_data, $variation){

		// $_jcsp_enabled = isset($variation_data['_jcsp_enabled'][0]) ? $variation_data['_jcsp_enabled'][0] : 'no';
		// $_jcsp_limit_min = isset($variation_data['_jcsp_limit_min'][0]) ? $variation_data['_jcsp_limit_min'][0] : '';
		// $_jcsp_suggested_price = isset($variation_data['_jcsp_suggested_price'][0]) ? $variation_data['_jcsp_suggested_price'][0] : '';

		// custom variation data
		$variation_id = $variation->ID;
		$_jcsp_enabled = get_post_meta( $variation_id, '_jcsp_enabled', true);
		$_jcsp_limit_min = get_post_meta( $variation_id, '_jcsp_limit_min', true);
		$_jcsp_suggested_price = get_post_meta( $variation_id, '_jcsp_suggested_price', true);
		?>
		
		<tr>
			<td colspan="2">
				<hr class="show_if_variable_is_jcsp"/>
				<div class="show_if_variable_is_jcsp form-row form-row-full">
				
					<p class="form-row form-row-first">
						<label><?php echo __( 'Minimum Price:', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')'; ?></label>
						<input type="text" size="5" name="_jcsp_variable_limit_min[<?php echo $loop; ?>]" value="<?php if ( isset( $_jcsp_limit_min ) ) echo esc_attr( $_jcsp_limit_min ); ?>" class="wc_input_price" />
					</p>
					<p class="form-row form-row-last">
						<label><?php echo __( 'Suggested Price:', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')'; ?></label>
						<input type="text" size="5" name="_jcsp_variable_suggested_price[<?php echo $loop; ?>]" value="<?php if ( isset( $_jcsp_suggested_price ) ) echo esc_attr( $_jcsp_suggested_price ); ?>" class="wc_input_price" />
					</p>

				</div>
			</td>
		</tr>
		<?php
	}

	/**
	 * Save simple product value
	 * @param  integer $post_id 
	 * @param  array $post    
	 * @return void
	 */
	public function save_product($post_id, $post){

		$enable_jcsp = isset($_POST['_jcsp_enabled']) ? 'yes' : 'no';
		update_post_meta( $post_id, '_jcsp_enabled', $enable_jcsp );

		// Update limit min
		$min = isset($_POST['_jcsp_limit_min']) && $_POST['_jcsp_limit_min'] >= 0 && $_POST['_jcsp_limit_min'] !== '' ? wc_format_decimal( $_POST['_jcsp_limit_min']) : 0;
		update_post_meta( $post_id, '_jcsp_limit_min', $min );

		// Update limit max
		$suggested = isset($_POST['_jcsp_suggested_price']) && $_POST['_jcsp_suggested_price'] >= 0 && $_POST['_jcsp_suggested_price'] !== '' ? wc_format_decimal( $_POST['_jcsp_suggested_price']) : 0;
		update_post_meta( $post_id, '_jcsp_suggested_price', $suggested );
	}

	/**
	 * Save variable product variation
	 * @param  integer $variation_id 
	 * @param  integer $i            
	 * @return void
	 */
	public function save_product_variation($variation_id, $i){

		$enable_jcsp = isset($_POST['_jcsp_variable_enabled'][$i]) ? 'yes' : 'no';
		update_post_meta( $variation_id, '_jcsp_enabled', $enable_jcsp );

		// Update limit min
		$min = isset($_POST['_jcsp_variable_limit_min'][$i]) && $_POST['_jcsp_variable_limit_min'][$i] >= 0 && $_POST['_jcsp_variable_limit_min'][$i] !== '' ? wc_format_decimal( $_POST['_jcsp_variable_limit_min'][$i]) : 0;
		update_post_meta( $variation_id, '_jcsp_limit_min', $min);
		// if ( isset( $_POST['_jcsp_variable_limit_min'][$i] ) ) {
		// 	update_post_meta( $variation_id, '_jcsp_limit_min', ( $_POST['_jcsp_variable_limit_min'][$i] === '' ) ? '' : wc_format_decimal( $_POST['_jcsp_variable_limit_min'][$i] ) );
		// }

		// Update limit max
		$suggested = isset($_POST['_jcsp_variable_suggested_price'][$i]) && $_POST['_jcsp_variable_suggested_price'][$i] >= 0 && $_POST['_jcsp_variable_suggested_price'][$i] !== '' ? wc_format_decimal( $_POST['_jcsp_variable_suggested_price'][$i]) : 0;
		update_post_meta( $variation_id, '_jcsp_suggested_price', $suggested );
		// if ( isset( $_POST['_jcsp_variable_suggested_price'][$i] ) ) {
		// 	update_post_meta( $variation_id, '_jcsp_suggested_price', ( $_POST['_jcsp_variable_suggested_price'][$i] === '' ) ? '' : wc_format_decimal( $_POST['_jcsp_variable_suggested_price'][$i] ) );
		// }
	}
}

new JCSP_Admin_Product_Settings();