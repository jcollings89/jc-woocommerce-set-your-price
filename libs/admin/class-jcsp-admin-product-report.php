<?php
add_filter( 'woocommerce_admin_reports', 'jcsp_woocommerce_admin_reports');

function jcsp_woocommerce_admin_reports($reports){

	$reports['jcsp'] = array(
		'title'  => __( 'Set Your Price', 'jcsp' ),
		'reports' => array(
			'product_prices' => array(
				'title' => __('Product Prices', 'jcsp'),
				'description' => '',
				'hide_title' => true,
				'callback' => 'jcsp_get_report'
			)
		)
	);

	return $reports;
}

function jcsp_get_report(){

	$start_date = isset($_GET['jcsp_start']) ? date('Y-m-d', strtotime($_GET['jcsp_start'])) : date('Y-m-\0\1');
	$end_date = isset($_GET['jcsp_end']) ? date('Y-m-d', strtotime($_GET['jcsp_end'])) : date('Y-m-t');

	?>
	<style type="text/css">
	.jcsp-report #poststuff{
		padding-top:0;
	}
	.jcsp-report .column-product{
		width: 30%;
	}
	</style>
	<div class="jcsp-report">
	<form method="GET" action="">
		<p>
		<input type="hidden" name="page" value="wc-reports" />
		<input type="hidden" name="tab" value="jcsp" />
		<label>Start Date: <input name="jcsp_start" value="<?php echo $start_date; ?>" /></label>
		<label>End Date: <input name="jcsp_end" value="<?php echo $end_date; ?>" /></label>
		<input type="Submit" />
		</p>
	</form>
	<?php

	$report = new JCSP_Product_Price_Report($start_date, $end_date);
	$report->output_report();
	?>
	</div>
	<?php
}

// echo WC_PLUGIN_BASENAME;

if ( ! class_exists( 'WC_Report_Stock' ) ) {
	require_once( WP_PLUGIN_DIR . '/woocommerce/includes/admin/reports/class-wc-report-stock.php' );
}

class JCSP_Product_Price_Report extends WC_Report_Stock {

	public $start_date = false;
	public $end_date = false;

	public function __construct($start_date, $end_date){

		parent::__construct( array(
			'singular'  => __( 'Set Your Price', 'jcsp' ),
			'plural'    => __( 'Set Your Price', 'jcsp' ),
			'ajax'      => false
		) );

		$this->start_date = isset($start_date) ? date('Y-m-d', strtotime($start_date)) : date('Y-m-\0\1');
		$this->end_date = isset($end_date) ? date('Y-m-d', strtotime($end_date)) : date('Y-m-t');
	}

	/**
	 * No items found text
	 */
	public function no_items() {
		_e( 'No Set Your Price products have been found in orders.', 'jcsp' );
	}

	/**
	 * column_default function.
	 *
	 * @param mixed $item
	 * @param mixed $column_name
	 */
	public function column_default( $item, $column_name ) {

		switch($column_name){
			case 'product':
				// parent::column_default($item, $column_name);
				if($item->variation_id > 0){
					$variation = new WC_Product_Variation($item->variation_id);
					echo $variation->get_formatted_name();
				}else{
					$product = new WC_Product($item->id);
					echo $product->get_title();
				}
			break;
			case 'price':
				echo wc_price($item->item_line_total / $item->item_order_qty);
			break;
			case 'qty':
				echo $item->item_order_qty;
			break;
			case 'min_price':
				echo wc_price($item->item_order_min);
			break;
			case 'max_price':
				echo wc_price($item->item_order_max);
			break;
			case 'orders':
				echo $item->item_order_count;
			break;
		}
	}

	/**
	 * Get Products matching stock criteria
	 */
	public function get_items( $current_page, $per_page ) {
		global $wpdb;

		$this->max_items = 0;
		$this->items     = array();

		$page = 0;
		if($current_page > 1){
			$page = $current_page * $per_page;
		}


		$date_query = "";
		if($this->start_date && $this->end_date){
			
			$date_query = "AND CAST(post.`post_date` AS DATE) >= CAST('{$this->start_date}' AS DATE)
			AND CAST(post.`post_date` AS DATE) <= CAST('{$this->end_date}' AS DATE)";
		}elseif($this->end_date){

			$date_query = "AND CAST(post.`post_date` AS DATE) <= CAST('{$this->end_date}' AS DATE)";

		}elseif($this->start_date){

			$date_query = "AND CAST(post.`post_date` AS DATE) >= CAST('{$this->start_date}' AS DATE)";
		}

		// fetch paginated list of order items with jcsp enabled
		$paginated_items_query = "SELECT 
				om2.`meta_value` AS id, 
				om5.`meta_value` AS variation_id,
				SUM((CAST(om3.`meta_value` AS UNSIGNED) / CAST(om4.`meta_value` AS UNSIGNED))) AS item_order_total, 
				COUNT(*) AS item_order_count, 
				SUM(om4.`meta_value`) AS item_order_qty, 
				MIN((CAST(om3.`meta_value` AS UNSIGNED) / CAST(om4.`meta_value` AS UNSIGNED))) AS item_order_min, 
				MAX((CAST(om3.`meta_value` AS UNSIGNED) / CAST(om4.`meta_value` AS UNSIGNED))) AS item_order_max,
				SUM(om3.`meta_value`) AS item_line_total
			FROM {$wpdb->prefix}woocommerce_order_itemmeta  AS om1
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS om2 ON om1.`order_item_id` = om2.`order_item_id`
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS om3 ON om2.`order_item_id` = om3.`order_item_id`
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS om4 ON om2.`order_item_id` = om4.`order_item_id`
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS om5 ON om2.`order_item_id` = om5.`order_item_id`
			INNER JOIN {$wpdb->prefix}woocommerce_order_items AS order_item ON om1.`order_item_id` = order_item.`order_item_id`
			INNER JOIN {$wpdb->prefix}posts AS post ON order_item.`order_id` = post.`ID`
			WHERE 1=1
			AND om1.`meta_key` = '_jcsp_enabled'
			AND om2.`meta_key` = '_product_id'
			AND om3.`meta_key` = '_line_total'
			AND om4.`meta_key` = '_qty'
			AND om5.`meta_key` = '_variation_id'
			{$date_query}
			GROUP BY om5.`meta_value`, om2.`meta_value`
			LIMIT {$page},{$per_page}";
		$result = $wpdb->get_results($paginated_items_query);

		// fetch item count of order items with jcsp enabled
		$max_items_query = "SELECT  COUNT(*) FROM {$wpdb->prefix}woocommerce_order_itemmeta  AS om1
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS om2 ON om1.`order_item_id` = om2.`order_item_id`
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS om5 ON om2.`order_item_id` = om5.`order_item_id`
			INNER JOIN {$wpdb->prefix}woocommerce_order_items AS order_item ON om1.`order_item_id` = order_item.`order_item_id`
			INNER JOIN {$wpdb->prefix}posts AS post ON order_item.`order_id` = post.`ID`
			WHERE 1=1
			AND om1.`meta_key` = '_jcsp_enabled'
			AND om2.`meta_key` = '_product_id'
			AND om5.`meta_key` = '_variation_id'
			{$date_query}
			GROUP BY om5.`meta_value`, om2.`meta_value`";

		$this->items = $result;
		$this->max_items = count($wpdb->get_results($max_items_query));
	}

	/**
	 * get_columns function.
	 */
	public function get_columns() {

		$columns = array(
			'product'      => __( 'Product', 'jcsp' ),
			'orders'      => __( 'Orders', 'jcsp' ),
			'qty'      => __( 'Items Sold', 'jcsp' ),
			'price'      => __( 'Avg', 'jcsp' ),
			'min_price'      => __( 'Min', 'jcsp' ),
			'max_price'      => __( 'Max', 'jcsp' ),
		);

		return $columns;
	}
}