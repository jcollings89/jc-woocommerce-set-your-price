<?php
/**
 * Display set your price fields on single and archive pages
 * 
 * @author James Collings <james@jclabs.co.uk>
 * @version 0.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class JCSP_Product_Template{

	public function __construct(){

		// output public form for enabled products
		add_action('woocommerce_before_add_to_cart_button', array( $this, 'display_field' ) );

		// process public form data
		add_action( 'woocommerce_add_to_cart', array( $this, 'add_cart_item_data'), 10, 6 );

		// validate add to cart
		add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'add_to_cart_validation'), 10, 4);

		// change price
		add_filter( 'woocommerce_get_price', array( $this, 'get_price' ), 10, 2 );

		// 'woocommerce_product_add_to_cart_text', $text, $this
		add_filter( 'woocommerce_product_add_to_cart_text' , array($this, 'add_to_cart_text'), 10, 2);

		// woocommerce_product_add_to_cart_url
		add_filter( 'woocommerce_product_add_to_cart_url' , array( $this, 'add_to_cart_url'), 10, 2);

		// woocommerce_loop_add_to_cart_link
		add_filter( 'woocommerce_loop_add_to_cart_link', array( $this, 'add_to_cart_link'), 10 , 2);
		
		// add data to order meta
		add_action( 'woocommerce_order_add_product', array( $this, 'order_add_product' ), 10, 3);

		if( ( isset( $_GET[ 'wc-ajax' ]) && $_GET['wc-ajax'] == 'add_to_cart' ) || ( isset( $_POST['add-to-cart'] ) && !empty( $_POST['add-to-cart'] ) ) ){

			// add to cart hook
			add_action( 'woocommerce_add_to_cart', array( $this, 'apply_price' ) );

		}elseif ( isset( $_POST[ 'update_cart' ] ) && !empty( $_POST[ 'update_cart' ] )  ) {

			// update basket hook
			add_action( 'woocommerce_update_cart_action_cart_updated', array( $this, 'apply_price' ) );

		}else{
			
			// basket load hook
			add_action('woocommerce_cart_loaded_from_session', array($this, 'apply_price'));
			
			// works when unit testing
			add_action('woocommerce_before_calculate_totals', array($this, 'apply_price')); 
		}
	}

	/**
	 * Add _jcsp_enabled to order_itemmeta table
	 * @param  int $order_id 
	 * @param  int $item_id  
	 * @param  object $product  
	 * @return void
	 */
	public function order_add_product($order_id, $item_id, $product){

		$product_id =  isset( $product->variation_id ) ? $product->variation_id : 0;
		if($product_id == 0){
			$product_id = $product->id;
		}

		if($product_id){

			$enabled = get_post_meta( $product_id, '_jcsp_enabled', true );
			if($enabled == 'yes'){
				wc_add_order_item_meta( $item_id, '_jcsp_enabled', 'yes');
			}
		}		
	}

	/**
	 * Change add to cart button text in product archive
	 * @param string $text    
	 * @param array $product
	 * @return string
	 */
	public function add_to_cart_text($text, $product){
		
		if($this->is_enabled($product->id)){
			return __('View Product', 'jcsp');
		}

		return $text;
	}

	/**
	 * Change add to cart url in product archive
	 * @param string $url     
	 * @param array $product 
	 * @return string
	 */
	public function add_to_cart_url($url, $product){
		
		if($this->is_enabled($product->id)){
			return $product->get_permalink();
		}

		return $url;
	}

	/**
	 * Remove add_to_cart_button class so its not treated as AJAX link
	 * @param string $url     
	 * @param array $product 
	 * @return string
	 */
	public function add_to_cart_link($url, $product){

		if($this->is_enabled($product->id)){
			return str_replace('add_to_cart_button', '', $url);
		}

		return $url;
	}

	/**
	 * Get suggested price for archive/single and on cart display set price
	 * @param  numeric $price   
	 * @param  array $product 
	 * @return numeric
	 */
	public function get_price($price, $product){

		if(!is_checkout() && !is_cart() && !defined('WOOCOMMERCE_CHECKOUT') && !defined('WOOCOMMERCE_CART') && !isset( $_GET[ 'wc-ajax' ])){
			
			if($product->variation_id !== '' && $this->is_enabled($product->variation_id)){
				$suggested = get_post_meta( $product->variation_id, '_jcsp_suggested_price', true );
				if($suggested !== false){
					return $suggested;
				}
			}elseif(($product->variation_id === '' || $product->variation_id === 0) && $this->is_enabled($product->id)){
				$suggested = get_post_meta( $product->id, '_jcsp_suggested_price', true );
				if($suggested !== false){
					return $suggested;
				}
			}
		}	

		return $price;
	}

	public function apply_price(){

		// get cart contents
		$cart_contents = WC()->cart->cart_contents;
		$jcsp_cart = WC()->session->get('jcsp_cart');

		foreach($cart_contents as $cart_key => $cart_item){

			$product_id = isset( $cart_item['product_id'] ) ? $cart_item['product_id'] : false;
			$variation_id = isset( $cart_item['variation_id'] ) ? $cart_item['variation_id'] : false;

			if( isset( $jcsp_cart[$cart_key]['price'] ) && $this->is_valid_amount( $jcsp_cart[$cart_key]['price'], $product_id, $variation_id ) ){
				$cart_contents[ $cart_key ][ 'data' ]->price = $jcsp_cart[$cart_key]['price'];
			}			
		}
	}

	/**
	 * On item being added to the cart, save to session the set price
	 * @param string $cart_item_key  
	 * @param integer $product_id     
	 * @param integer $quantity       
	 * @param integer $variation_id   
	 * @param array $variation      
	 * @param array $cart_item_data
	 * @return void
	 */
	public function add_cart_item_data($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data){

		if( ( $this->is_enabled( $variation_id ) ) || ($this->is_enabled( $product_id ) && empty($variation_id)) ){

			// get price
			$set_price = wc_clean($_POST['jcsp_price']);

			// update jcsp_cart session
			$jcsp_cart = WC()->session->get('jcsp_cart');
			if(!is_array($jcsp_cart)){
				$jcsp_cart = array();
			}

			if($variation_id){
				$jcsp_cart[$cart_item_key] = array(
					'price' => $set_price,
					'variation_id' => $variation_id
				);
			}else{
				$jcsp_cart[$cart_item_key] = array(
					'price' => $set_price,
					'product_id' => $product_id,
				);
			}

			WC()->session->set('jcsp_cart', $jcsp_cart);
		}
	}

	/**
	 * Get simple/variable product price from jcsp_cart session
	 * @param  int  $product_id   
	 * @param  int $variation_id 
	 * @return mixed
	 */
	public function get_session_price($product_id, $variation_id = false){

		$jcsp_cart = WC()->session->get('jcsp_cart');
		if(is_array($jcsp_cart)){

			foreach($jcsp_cart as $cart_item_key => $item){

				if($variation_id){

					if(isset($item['variation_id']) && $item['variation_id'] == $variation_id){
						return $item['price'];	
					}
				}else{

					if(isset($item['product_id']) && $item['product_id'] == $product_id){
						return $item['price'];	
					}
				}
			}
		}

		return false;
	}


	/**
	 * Validate add to cart action for set your price products
	 * @param boolean $valid      
	 * @param integer $product_id 
	 * @param integer $quantity   
	 */
	public function add_to_cart_validation($valid, $product_id, $quantity, $variation_id = ''){
		
		if(!empty($variation_id) && $this->is_enabled( $variation_id )){

			$set_price = wc_clean($_POST['jcsp_price']);
			if($this->is_valid_amount($set_price, $product_id, $variation_id)){
				return true;
			}

			return false;		

		}elseif(empty($variation_id) && $this->is_enabled($product_id)){

			$set_price = wc_clean($_POST['jcsp_price']);
			if($this->is_valid_amount($set_price, $product_id)){
				return true;
			}

			return false;			
		}

		return $valid;
	}

	/**
	 * Make sure the amount passed is within product limits
	 * @param  numeric  $amount     
	 * @param  integer  $product_id 
	 * @return boolean
	 */
	public function is_valid_amount($amount, $product_id, $variation_id = false){

		$min_amount = '';

		if(!is_numeric($amount)){
			wc_add_notice( JCSP()->get_label( 'notice', 'error' ) , 'error' );
			return false;
		}

		// get variation limits
		if ( $variation_id ) {

			$min_amount = get_post_meta( $variation_id, '_jcsp_limit_min', true );
		}

		if($min_amount === ''){
			$min_amount = get_post_meta( $product_id, '_jcsp_limit_min', true );	
		}
		

		// no min set, will be set to 0
		if($min_amount === '' || !is_numeric($min_amount)){
			$min_amount = 0;
		}


		// is amount greater than minimum and less than max if set
		if(($amount >= $min_amount)){
			return true;
		}

		// todo: replace 2 vars with wc_price?
		wc_add_notice( sprintf(JCSP()->get_label( 'notice', 'min_error' ), wc_price($min_amount)) , 'error');
		return false;
	}

	/**
	 * Check to see if the product has set your price enabled
	 * @param  integer  $product_id 
	 * @return boolean
	 */
	public function is_enabled($product_id){
		$enabled = get_post_meta( $product_id, '_jcsp_enabled', true );

		if($enabled && $enabled == 'yes'){
			return true;
		}

		return false;
	}

	/**
	 * fetch the correct layout to be displayed, simple or variable product
	 * @return void
	 */
	public function display_field(){
		global $product;

		if($product->product_type == 'variable'){
			return $this->display_variable_field($product);
		}elseif($product->product_type == 'simple'){
			return $this->display_simple_field($product);
		}
	}

	/**
	 * Output form for variable products
	 * @param  array $product 
	 * @return void
	 */
	public function display_variable_field($product){

		if(empty($product->children)){
			return false;
		}

		$json = array();
		foreach($product->children['visible'] as $child){
			if(!$this->is_enabled($child)){
				continue;
			}

			$suggested_price = get_post_meta( $child, '_jcsp_suggested_price', true );
			$min_amount = get_post_meta( $child, '_jcsp_limit_min', true );
			
			// check for session price, if not use suggested price
			$price = false; //$this->get_session_price( $product->id, $child );
			if(!$price){
				$price = $suggested_price;
			}

			$json[$child] = array(
				'id' => $child,
				'suggested' => $suggested_price,
				'minimum' => $min_amount,
				'price' => $price
			);
		}

		include jcsp_get_template_part('product-variation');
	}

	/**
	 * Output form for simple products
	 * @param  array $product 
	 * @return void
	 */
	public function display_simple_field($product){
		
		// escape if not enabled for product
		if(!$this->is_enabled($product->id)){
			return;
		}

		$suggested_price = get_post_meta( $product->id, '_jcsp_suggested_price', true );
		$min_amount = get_post_meta( $product->id, '_jcsp_limit_min', true );

		// check for session price, if not use suggested price
		$price = $this->get_session_price($product->id);
		if(!$price){
			$price = $suggested_price;
		}

		include jcsp_get_template_part('product-simple');
	}
}

new JCSP_Product_Template();