module.exports = function(grunt) {

    var currDate = new Date();

    var day = currDate.getUTCDate();
    var month = (currDate.getUTCMonth() + 1);
    if(day < 10){
        day = '0' + day;
    }
    if(month < 10){
        month = '0' + month;
    }

    var formattedDate = day + '/' + month + '/'+currDate.getUTCFullYear()

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            main: {
                files: [
                    {expand: true, src: ['libs/**'], dest: 'build/tmp/'},
                    {expand: true, src: ['templates/**'], dest: 'build/tmp/'},
                    {expand: true, src: ['assets/**'], dest: 'build/tmp/'},
                    {expand: false, src: ['README.md'], dest: 'build/tmp/README.md'},
                    {expand: false, src: ['README.html'], dest: 'build/tmp/README.html'},
                    {expand: false, src: ['jc-woocommerce-set-your-price.php'], dest: 'build/tmp/jc-woocommerce-set-your-price.php'}
                ]
            }
        },
        clean: {
            build: ["build/tmp/*", "!build/.svn/*"],
        },
        compress: {
            main: {
                options: {
                    archive: 'build/<%= pkg.name %>-v<%= pkg.version %>.zip'
                },
                files: [
                    {cwd: 'build/tmp/', src: ['**'], dest: '<%= pkg.name %>/'},
                ]
            }
        },
        'string-replace': {
            dist:{
                files:[{
                  expand: true,
                  cwd: './',
                  src: ['<%= pkg.name %>.php', 'README.md']
                  // dest: '<%= pkg.name %>.php'
                }],
                options:{
                    replacements: [
                        {
                            pattern: /Version: ([0-9a-zA-Z\-\.]+)/m,
                            replacement: 'Version: <%= pkg.version %>'
                        },
                        {
                            pattern: /\$version = '([0-9a-zA-Z\-\.]+)';/m,
                            replacement: '$version = \'<%= pkg.version %>\';'
                        },
                        {
                            pattern: /^Updated: ([0-9\/]+)/m,
                            replacement: 'Updated: '+formattedDate
                        }
                    ]
                }
            }
        }
    });

    // grunt modules
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-string-replace');

    // Default task(s).
    grunt.registerTask('default', ['string-replace', 'clean:build', 'copy', 'compress'/*, 'clean:build'*/]);

};