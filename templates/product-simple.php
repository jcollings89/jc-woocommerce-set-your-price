<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="jcsp-field simple">
	<label><?php echo JCSP()->get_label('general', 'before_input'); ?></label>
	<input type="text" name="jcsp_price" id="jcsp_price" value="<?php echo $price ? $price : ''; ?>" />
	<?php if($min_amount > 0 && apply_filters( 'jcsp_show_min_price', true )): ?>
	<p class="jcsp-minimum"><em><?php echo JCSP()->get_label('general', 'after_input'); ?> <span><?php echo $min_amount ?></span></em></p>
	<?php endif; ?>
</div>