<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$show_minimum = apply_filters( 'jcsp_show_min_price', true );

?>
<script type="text/javascript">
jQuery(function($){

	var _variation_data = <?php echo json_encode($json); ?>;
	var show_minimum = <?php echo $show_minimum == true ? 'true' : 'false'; ?>;

	$('.jcsp-field').hide();

	// temp fix to hide field on clear selection
	$('body').on( 'change', '.variations select', function( event ) {
		
		$('.jcsp-field').hide();

		var _variation = parseInt($('input[name^="variation_id"]').val());
		if( _variation > 0 && _variation_data[_variation] !== undefined){
			$('.jcsp-field').show();
		}
	});

	$('.single_variation_wrap').on('show_variation', function(){

		$('.single_variation_wrap .price').show();
		var _variation = parseInt($('input[name^="variation_id"]').val());

		if( _variation > 0 && _variation_data[_variation] !== undefined){

			$('.jcsp-field').show();
			variation = _variation_data[_variation];
			
			if( variation['price'] > 0){
				$('#jcsp_price').val(variation['price']);
			}else{
				$('#jcsp_price').val('');
			}

			if(variation['minimum'] > 0 && show_minimum){
				$('.jcsp-minimum').show();
				$('.jcsp-minimum span').text(variation['minimum']);
			}else{
				$('.jcsp-minimum').hide();
			}

			$(this).find('.price').hide();
		}
	});
});
</script>

<div class="jcsp-field">
	<label><?php echo JCSP()->get_label('general', 'before_input'); ?></label>
	<input type="text" name="jcsp_price" id="jcsp_price" value="" />
	<p class="jcsp-minimum"><em><?php echo JCSP()->get_label('general', 'after_input'); ?> <span></span></em></p>
</div>