#JC WooCommerce Set Your Price Plugin
Author: James Collings 
Version: 0.2.5
Created: 16/12/2014
Updated: 01/02/2016

##About
JC WooCommerce Set Your Price plugin allows WooCommerce products to be sold for what users think they are worth, with the ability to set a minimum and recommeneded price.

##Features
* Allow users to set their own price for how much a product is worth to them
* Set the minimum price allowed so as not to give products away to cheaply
* Set a minimum and a recommended price for the product

##Installation
1. Download a copy of the plugin
1. Copy the jc-woocommerce-set-your-price folder to your wordpress plugins directory, usually /wp-content/plugins/
1. Goto plugins page within Wordpress and activate JC WooCommerce Set Your Price plugin

##Documentation

__Table of Contents__

1. [Enable Set Your Price on a simple product](#enable-set-your-price-on-a-simple-product)
1. [Enable Set Your Price on a variable product](#enable-set-your-price-on-a-variable-product)
1. [Set Your Price General Settings](#general-settings)
1. [Set Your Price Product Report](#set-your-price-product-report)

### Enable Set Your Price on a simple product

To enable set your price on a simple product, scroll down to the product data section within the edit screen for the selected product.

![Disabled Settings](./assets/jcsp-simple-product-disabled-settings.png)

Toggle on the checkbox highlighted below, this will hide the default pricing and sales fields and they will be replaced by a minimum and suggested price fields.

__Minimum Price__
The minumum price field defaults to 0, enter the cheapest value that customers can enter

__Suggested Price__
This field will autopopulate the Set your price product field to entered amount, and will be displayed as the product price.

![Enabled Settings](./assets/jcsp-simple-product-enabled-settings.png)

Shown below is the output for a Set Your Price product that has a minimum price of 5 and a suggested amount of 10 (please note that text can be changed on the output, please read the labels section of the documentation).

![Simple Product View](./assets/jcsp-simple-product-view.png)

### Enable Set Your Price on a variable product

When using variable products with Set Your Price you can enable it seperatly on each individual product. To enable set your price on a variable product, scroll down to the product data section within the edit screen for the selected product, click on the variations tab down the left hand side, Expand the variation that you want to enable it on.

![Disabled Settings](./assets/jcsp-variable-product-disabled-settings.png)

Toggle on the checkbox highlighted below, this will hide the default pricing and sales fields and they will be replaced by a minimum and suggested price fields.

__Minimum Price__
The minumum price field defaults to 0, enter the cheapest value that customers can enter

__Suggested Price__
This field will autopopulate the Set your price product field to entered amount, and will be displayed as the product price.

![Enabled Settings](./assets/jcsp-variable-product-enabled-settings.png)

### General Settings

![Enabled Settings](./assets/jcsp-admin-general-settings.png)

__Enable Label Override__
if checked this will add a new settings tab called Labels , where you can modify all the plugins text

__Enable Plugin Styles__
if checked the default plugin css styles will be loaded

### Label Settings

![Enabled Settings](./assets/jcsp-admin-label-settings.png)

#### General Labels
Text which is displayed on the front end of the plugin

![Enabled Settings](./assets/jcsp-admin-label-settings-general.png)

__Before input__
The text which appears before the set your price amount input on the single product view.

__After input__
The text which appears after the set your price amount input on the single product view.

#### Notice Labels
Text which alerts the users of errors or validation

![Enabled Settings](./assets/jcsp-admin-label-settings-notice.png)

__General Error__
Notice text to display if anything goes wrong when processing the amount.

__Minimum validation error__
Notice text which is displayed on entered amount that is below the minimum amount.

### Set Your Price Product Report

To see a report on what customers are choosing to pay for your products goto WooCommerce > Reports and click on the Set Your Price tab.

![Report Settings](./assets/jcsp-admin-report.png)

To set the time period that you would like to view a product report on, set the start and end date input fields which are shown above the product list and click on Submit to view the result.

## Changelog

### 0.2
* Add product reports section
* Minor Improvements and Bug fixes

### 0.1
* Plugin released