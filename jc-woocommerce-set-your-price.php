<?php
/*
	Plugin Name: JC WooCommerce Set Your Price
	Plugin URI: http://www.jamescollings.co.uk/wordpress-plugins/woocommerce-set-your-price
	Description: Allow customers to name their price on specific products
	Version: 0.2.5
	Author: James Collings
	Author URI: http://www.jamescollings.co.uk
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class JCWC_SetYourPrice {

	protected $version = '0.2.5';
	public $plugin_dir = false;
	public $plugin_url = false;
	protected $plugin_slug = false;
	protected $labels = array();
	protected $labels_default = array();
	protected $disable_css = false;

	/**
	 * Single instance of class
	 */
	protected static $_instance = null;

	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function __construct(){

		$this->plugin_dir =  plugin_dir_path( __FILE__ );
		$this->plugin_url = plugins_url( '/', __FILE__ );
		$this->plugin_slug = basename(dirname(__FILE__));

		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts' ));

		add_action( 'woocommerce_init', array( $this, 'init' ) );
	}

	public function includes(){

		include_once $this->get_plugin_dir() . 'libs/jcsp-functions.php';

		if(is_admin()){
			include_once 'libs/admin/jcsp-admin-init.php';
		}
		
		include_once 'libs/class-jcsp-product-template.php';
	}

	/**
	 * Initite donation
	 * @return void
	 */
	public function init(){

		$this->labels_default = array(
			'general' => array(
				'before_input' => __('Name Your Price', 'jcsp'),
				'after_input' => __('Minimum Amount:', 'jcsp')
			),
			'notice' => array(
				'error' => __( "Please enter a valid amount", 'jcsp'),
				'min_error' => __( "Please enter a value above %s", 'jcsp'),
			)
		);

		$this->includes();
		$this->load_settings();	
	}

	/**
	 * Load plugin settings
	 * @return void
	 */
	public function load_settings(){

		if(get_option('jcsp_enable_labels') == 'yes'){

			// load labels
			foreach($this->labels_default as $section_id => $section){
				foreach($section as $label_id => $label){
					$new_label = get_option("jcsp_{$section_id}_{$label_id}");
					if($new_label){
						$this->labels_default[$section_id][$label_id] = $new_label;
					}
				}
			}
		}
	}

	public function get_version(){
		return $this->version;
	}
	public function get_plugin_slug(){
		return $this->plugin_slug;
	}
	public function get_plugin_dir(){
		return $this->plugin_dir;
	}

	/**
	 * Fetch plugin labels
	 * 
	 * @param  string $section 
	 * @param  integer $id      
	 * @return mixed/string
	 */
	public function get_label($section, $id, $default = false){

		if(!$default && get_option('jcsp_enable_labels') == 'yes'){

			// return modified labels
			if(isset($this->labels[$section][$id]) && !empty($this->labels[$section][$id])){
				return $this->labels[$section][$id];
			}
		}

		// return default labels
		if(isset($this->labels_default[$section][$id])){
			return $this->labels_default[$section][$id];
		}		
		
		return false;
	}

	/**
	 * Add stylesheet to single product page
	 * @return void
	 */
	public function enqueue_scripts(){

		// temp fix for wpml as it doesnt know when its on a product page
		if(!is_shop() || JCSP()->disable_css){
			return;
		}

		wp_enqueue_style( 'jcsp-public', JCSP()->plugin_url . 'assets/css/jcsp-public.css' );
	}
}

function JCSP() {
	return JCWC_SetYourPrice::instance();
}

$GLOBALS['jcsp'] = JCSP();